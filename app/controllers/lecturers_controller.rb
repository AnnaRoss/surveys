class LecturersController < ApplicationController
    before_action :set_lecturer, only: [:show, :edit, :update, :destroy]
    
  def new
    @lecturer = Lecturer.new
    @lecturers = Lecturer.all
  end
  
  def create 
    @lecturers = Lecturer.all
    @lecturer = Lecturer.new(lecturer_params)
    respond_to do |format|
      if @lecturer.save
        format.html { flash[:notice] = 'Lecturer was successfully created.' and redirect_to action: "new" }
        format.json { render :new, status: :created, location: @lecturer }
      else
        format.html { render :new }
        format.json { render json: @lecturer.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def show
  end
  
  def update
    respond_to do |format|
      if @lecturer.update(lecturer_params)
        format.html { flash[:notice] = 'Lecturer name was successfully updated.' and redirect_to action: "new"}
        format.json { render :show, status: :ok, location: @lecturer }
      else
        format.html { render :edit }
        format.json { render json: @lecturer.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def destroy
    @lecturer.destroy
    respond_to do |format|
      format.html  { flash[:notice] = 'Lecturer was deleted.' and redirect_to new_lecturer_path }
      format.json { head :no_content }
    end
  end
  
  private
  
  # Use callbacks to share common setup or constraints between actions.
    def set_lecturer
      @lecturer = Lecturer.find(params[:id])
    end
    
    # Never trust parameters from the scary internet, only allow the white list through.
    def lecturer_params
      params.require(:lecturer).permit(:name)
    end
end
