class SessionsController < ApplicationController
    before_action :set_session, only: [:show, :edit, :update, :destroy]
  
  def index
    @sessions = Session.all
  end
  
  def show
  end
  
  def new
    @session = Session.new
    @sessions = Session.all
  end
  
  def create 
    @session = Session.new(session_params)
    respond_to do |format|
      if @session.save
        format.html { flash[:notice] = 'Session was successfully created.' and redirect_to action: "index" }
        format.json { render :index, status: :created, location: @session }
      else
        format.html { render :new }
        format.json { render json: @session.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def update
    respond_to do |format|
      if @session.update(session_params)
        format.html { flash[:notice] = 'Session was successfully updated.' and redirect_to action: "index" }
        format.json { render :show, status: :ok, location: @session }
      else
        format.html { render :edit }
        format.json { render json: @session.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def destroy
    @session.destroy
    respond_to do |format|
      format.html  { flash[:notice] = 'Session was deleted.' and redirect_to action: "index" }
      format.json { head :no_content }
    end
  end
  
  private
  
  # Use callbacks to share common setup or constraints between actions.
    def set_session
      @session = Session.find(params[:id])
    end
    
    # Never trust parameters from the scary internet, only allow the white list through.
    def session_params
      params.require(:session).permit(:title, :date, lecturer_ids: [])
    end
  
end
