class SurveysController < ApplicationController
  before_action :set_survey, only: [:destroy]
  before_action :get_sessionid, only: [:destroy]
  
  def new
    @session= Session.find(params[:id])
    @survey = @session.surveys.build
    @surveys = Survey.all
  end
  
  def create
     @session= Session.find(params[:id])
     @survey = @session.surveys.build(survey_params)
     if @survey.save
       redirect_to new_survey_path(:id => @session.id)
     end
  end
  
  def destroy
    @survey.destroy
    respond_to do |format|
      format.html  { flash[:notice] = 'Survey was deleted.' and redirect_to new_survey_path(:id => @sessionid)}
      format.json { head :no_content }
    end
  end
  
  
  private
  
    def set_survey
      @survey = Survey.find(params[:id])
    end
    
    def survey_params
      params.require(:survey).permit(:rating, :session_id)
    end
    
    def get_sessionid
      @sessionid = @survey.session_id
    end

    
  
end
