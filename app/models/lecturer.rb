class Lecturer < ApplicationRecord
  has_many :teachings
  has_many :sessions, through: :teachings
  
  validates :name, presence: true
end
