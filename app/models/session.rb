class Session < ApplicationRecord
  has_many :teachings
  has_many :lecturers, through: :teachings
  has_many :surveys
  
 validates :title, presence: true
 validates :date, presence: true
 validates :lecturers, presence:true
 
 validate :date_cannot_be_in_the_future
 
  def date_cannot_be_in_the_future
    if date.present? && date > Date.today
      errors.add(:date, "can't be in the future")
    end
  end  
end  
