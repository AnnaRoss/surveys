Rails.application.routes.draw do
  resources :lecturers
  resources :sessions
  resources :surveys

  root 'static_pages#home'
  get ':page', to: 'static_pages#show', as: 'pages'

  end
