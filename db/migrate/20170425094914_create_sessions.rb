class CreateSessions < ActiveRecord::Migration[5.0]
  def change
    create_table :sessions do |t|
      t.string :title
      t.string :lecturer
      t.date :date

      t.timestamps
    end
  end
end
