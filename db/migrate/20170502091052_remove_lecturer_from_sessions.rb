class RemoveLecturerFromSessions < ActiveRecord::Migration[5.0]
  def change
    remove_column :sessions, :lecturer, :string
  end
end
